import React, {useState, useEffect} from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Home from './src/screens/Home';
import DetailKata from './src/screens/DetailKata';
import Profile from './src/screens/Profile';
import DaftarKata from './src/screens/DaftarKata';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="DetailKata" component={DetailKata} />
        <Stack.Screen name="Profile" component={Profile} />
        <Stack.Screen name="DaftarKata" component={DaftarKata} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
