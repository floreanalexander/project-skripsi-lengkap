import React, {useState, useEffect} from 'react';
import {Text, View, StatusBar, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {useNavigation} from '@react-navigation/native';
const HeaderDetail = props => {
  const navigation = useNavigation();

  return (
    <View
      style={{
        backgroundColor: '#fefeff',
        elevation: 2,
        paddingVertical: 14,
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 10,
      }}>
      <StatusBar backgroundColor={'#fefeff'} barStyle="dark-content" />
      <TouchableOpacity
        style={{marginRight: 5}}
        onPress={() => navigation.goBack()}>
        <Icon name="arrow-back-circle" size={30} color={'#6a8dff'} />
      </TouchableOpacity>

      <View>
        <Text style={{fontWeight: 'bold', fontSize: 18}}>{props.title}</Text>
        <Text style={{fontSize: 14}}>{props.description}</Text>
      </View>
    </View>
  );
};

export default HeaderDetail;
