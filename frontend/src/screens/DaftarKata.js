import React, {useState, useEffect} from 'react';
import {View, Text, FlatList, Image} from 'react-native';
import BottomMenu from '../components/BottomMenu';
import HeaderDetail from '../components/HeaderDetail';
import {FlatGrid} from 'react-native-super-grid';
// import data from '../data/test.json';

import axios from "axios";

const DaftarKata = () => {
  const [datas, setData] = useState([]);
  const getDatas = async () => {
    const response = await axios.get("http://localhost:5000/datas");
    setData(response.data);
  };

  useEffect(() => {
    getDatas();
  }, []);
  
  return (
    <View style={{flex: 1}}>
      <HeaderDetail
        title="Daftar Kata"
        description="daftar kata yang ada di aplikasi ini"
      />
      <View style={{flex: 1}}>
        <FlatGrid
          data={datas}
          itemDimension={130}
          renderItem={({item}) => (
            <View
              style={{
                backgroundColor: '#fefeff',
                elevation: 2,
                borderRadius: 3,
                padding: 10,
              }}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  style={{width: 100, height: 100, borderRadius: 10}}
                  source={{uri: item.image}}
                />
              </View>
              <Text style={{fontWeight: 'bold', marginTop: 10}}>
                {item.indonesia}
              </Text>
              <Text>{item.english}</Text>
            </View>
          )}
        />
      </View>
      <BottomMenu screenName={'DaftarKata'} />
    </View>
  );
};

export default DaftarKata;
